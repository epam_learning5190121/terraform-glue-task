import * as AWSMock from 'aws-sdk-mock';
import * as AWS from 'aws-sdk';
import { getZipFileFromS3 } from './index'

describe('getZipFileFromS3', () => {
  let mockGetObject = jest.fn();

  beforeAll(() => {
    (AWSMock as any).setSDKInstance(AWS);
    (AWSMock as any).mock('S3', 'getObject', mockGetObject);
  });

  afterAll(() => {
    (AWSMock as any).restore('S3');
  });

  it('fetches zip file from S3 bucket', async () => {
    process.env.BUCKET = 'test-bucket';
    process.env.KEY = 'test-key';
    const testBody = Buffer.from("test content");
    mockGetObject.mockResolvedValue({ Body: testBody });

    const result = await getZipFileFromS3();
    expect(result).toEqual(testBody.toString('base64'));
    expect(mockGetObject).toHaveBeenCalledWith({
      Bucket: 'test-bucket',
      Key: 'test-key'
    });
  });

  it('throws an error when zip file not found in S3 bucket', async () => {
    process.env.BUCKET = 'test-bucket';
    process.env.KEY = 'test-key';
    mockGetObject.mockResolvedValue({});

    await expect(getZipFileFromS3()).rejects.toThrow('Could not retrieve the object from S3');
  });

  it('throws an error when ENV variables not set', async () => {
    delete process.env.BUCKET;
    delete process.env.KEY;
    
    await expect(getZipFileFromS3()).rejects.toThrow('Please provide Bucket name and Key in ENV variables');
  });
});
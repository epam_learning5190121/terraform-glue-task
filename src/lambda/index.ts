
import { S3, SES, SSM } from 'aws-sdk';

const sendMail = async (emails: Map<string, string> , encodedFile: any): Promise<any> => {

  const ses = new SES({ region: 'us-east-1' });

  const params = {
    RawMessage: {
      Data: Buffer.from(
        `From: ${emails.get("sender")}\n` +
        `To: ${emails.get("receiver")}\n` +
        `Subject: Report: tariff.zip file\n` +
        `MIME-Version: 1.0\n` +
        `Content-Type: multipart/mixed; boundary=NextPart\n\n` +
        
        `--NextPart\n` +
        `Content-Type: text/plain; charset=us-ascii\n\n` +
        `Hello Akshit, Please find the attached tariff.zip file\n\n` +
        
        `--NextPart\n` +
        `Content-Type: application/zip; name=tarrif.zip\n` +
        `Content-Description: tarrif.zip\n` +
        `Content-Disposition: attachment; filename=tarrif.zip;\n` +
        `Content-Transfer-Encoding: base64\n\n` +
        
        `${encodedFile}\n\n` +
        
        `--NextPart--`
      )
    }
  }

  // Send the email
  try {
    const emailResponse = await ses.sendRawEmail(params).promise();
  
    return emailResponse;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

const getEmailFromParameterStore = async (): Promise<any> => {

  const ssm = new SSM();
  let emails = new Map<string, string>();

  const receiverParameterName = 'receiverEmail'
  const receiver = await ssm.getParameter({ Name: receiverParameterName, WithDecryption: true }).promise();
    if (receiver.Parameter && receiver.Parameter.Value) {
      emails.set("receiver", receiver.Parameter.Value);
    } else { throw new Error(`Parameter ${receiverParameterName} not found`);}

  const senderParameterName = 'senderEmail'
  const sender = await ssm.getParameter({ Name: senderParameterName, WithDecryption: true }).promise();
    
    if (sender.Parameter && sender.Parameter.Value) {
      emails.set("sender", sender.Parameter.Value);
    } else { throw new Error(`Parameter ${senderParameterName} not found`);}  
    
    return emails;
}

export const getZipFileFromS3 = async (Key: string): Promise<any> => {
  const s3 = new S3();
  let Bucket: string;

  if (process.env.BUCKET !== undefined){
    Bucket = process.env.BUCKET;
  } else{
    throw new Error("Please provide Bucket name in ENV variables");
  }

  const s3Object = await s3.getObject({ Bucket, Key }).promise(); // Fetch the file from S3
  if (!s3Object.Body) {
    throw new Error('Could not retrieve the object from S3');
  }
  
  return s3Object.Body.toString('base64') // return Base64 encoded zip file
};


export const handler = async (event: any = {}): Promise<any> => {

  let key = event['--zipLocation'];
  const zipFile = await getZipFileFromS3(key);
  const emailIDs = await getEmailFromParameterStore().catch(console.error);
  await sendMail(emailIDs, zipFile);
  return {'body': 'Mail sent successfully..'}
};
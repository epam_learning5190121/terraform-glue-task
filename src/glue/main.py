import io
import boto3
import sys
import zipfile
from awsglue.utils import getResolvedOptions

def handler(bucket, objects_list, zip_location):
    s3 = boto3.client("s3")

    return_body = read_s3_files(s3, bucket, objects_list)
    zipped_file = file_zipper(s3, bucket, objects_list)

    try:
        s3.put_object(Bucket=bucket, Key=zip_location, Body=zipped_file)
    except Exception as err:
        print(err)
    
    return {
        'zip_location' : zip_location,
        'body' : return_body
    }
        

def read_s3_files(s3, bucket, objects_list):
    """
    Read multiple files from an S3 bucket and return their contents.

    Parameters:
    s3: An instance of a boto3 S3 client.
    bucket: The name of the S3 bucket.
    objects_list: A list of keys. Each key corresponds to an object in the S3 bucket.

    Returns:
    dict: A dictionary where each key-value pair corresponds to the name of a file in the S3 bucket and its contents.
    """
    return_body = {}

    for key in objects_list:
        data = s3.get_object(Bucket=bucket, Key=key)
        file_content = data['Body'].read().decode('utf-8')
        return_body[key] = file_content
    return return_body


def file_zipper(s3, bucket, objects_list):
    """
    This function creates a .zip file from a list of files (objects) in an S3 bucket.

    Parameters:
        s3: An instance of a boto3 S3 client.
        bucket: The name of the S3 bucket.
        objects_list: A list of keys. Each key corresponds to an object in the S3 bucket.

    Returns:
        bytes: The content of the created .zip file as bytes, which can then be used to write the .zip file locally or to another S3 bucket.
    """
    zip_buffer = io.BytesIO()
    with zipfile.ZipFile(zip_buffer, "a") as zipper:
        for obj in objects_list:
            infile_object = s3.get_object(Bucket=bucket, Key=obj)
            infile_content = infile_object['Body'].read()
            zipper.writestr(obj.split('/')[-1], infile_content)
    return zip_buffer.getvalue()

    
if __name__ == '__main__':
    ssm = boto3.client('ssm')
    response = ssm.get_parameter(Name='files')
    keys = response['Parameter']['Value'].split(',')   #comma separated list of files to be zipped
    args = getResolvedOptions(sys.argv, ['zipLocation', 'bucketName'])
    
    handler(args['bucketName'], keys, args['zipLocation'])
import boto3
import io
import unittest
import zipfile
from moto import mock_aws
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
from main import file_zipper, read_s3_files


class TestFileZipper(unittest.TestCase):

    @mock_aws
    def test_zipFile(self):
        bucket_name = 'test_bucket'
        s3 = boto3.client('s3', region_name='us-east-1')
        s3.create_bucket(Bucket=bucket_name)
        s3.put_object(Bucket=bucket_name, Key='sample_file.txt', Body=b'Test file content')
        objects_list = ['sample_file.txt']

        # Call to actual function
        result = file_zipper(s3, bucket_name, objects_list)
        # reconstruct the zip file to verify the contents
        zipfile_obj = zipfile.ZipFile(io.BytesIO(result), 'r')

        # Assert statements

        self.assertIs(zipfile_obj.testzip(), None)  # testzip returns None if no errors are found
        self.assertEqual(zipfile_obj.namelist(), objects_list)  # check if files in zip are same as the ones uploaded


class TestReadS3Files(unittest.TestCase):

    @mock_aws
    def test_read_s3_files(self):
        bucket_name = 'test-bucket'
        s3 = boto3.client('s3', region_name='us-east-1')
        s3.create_bucket(Bucket=bucket_name)

        # now let's store some "files" in the bucket
        files = {
            'file1.txt': 'Hello World!',
            'file2.txt': 'Hello Moto!'
        }

        for file_name, content in files.items():
            s3.put_object(Bucket=bucket_name, Key=file_name, Body=content)

        # call read_s3_files to fetch the file contents
        result = read_s3_files(s3, bucket_name, list(files.keys()))

        # assert that the result matches the file contents
        for file_name, content in files.items():
            self.assertEqual(result[file_name], content)

if __name__ == '__main__':
    unittest.main()
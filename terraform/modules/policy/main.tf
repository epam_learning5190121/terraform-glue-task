terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.47.0"
    }
  }
}

resource "aws_iam_policy" "invoke_lambda" {
  name        = "InvokeLambdaPolicy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "lambda:InvokeFunction",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "glue_start_job" {
  name = "GlueStartJobRunPolicy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "glue:*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "s3_access_policy" {
  name = "s3_access_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "s3:*",
      "Resource": "arn:aws:s3:::*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "send_mail_policy" {
  name = "send_mail_policy"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ses:*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "read_parameters_policy" {
  name = "read_parameters_policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ssm:GetParameter",
      "Resource": "*"
    }
  ]
}
EOF
}

output "s3_access_policy" {
  value = aws_iam_policy.s3_access_policy.arn
}

output "glue_run_policy" {
  value = aws_iam_policy.glue_start_job.arn
}

output "send_mail_policy" {
  value = aws_iam_policy.send_mail_policy.arn
}

output "read_parameters_policy" {
  value = aws_iam_policy.read_parameters_policy.arn
}

output "invoke_lambda_policy" {
  value = aws_iam_policy.invoke_lambda.arn
}
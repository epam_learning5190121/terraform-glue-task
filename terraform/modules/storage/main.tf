terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.47.0"
    }
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "glue-learn-task-using-tf"  # Your unique bucket name
  force_destroy = true
}

resource "aws_s3_object" "object1" {
  bucket = aws_s3_bucket.bucket.id  # Your bucket name
  key    = "tariff/tariff.dat"  # Desired S3 destination path with the object name
  source = "./modules/storage/tariff.dat"  # Path to the source file in local
}

resource "aws_s3_object" "object2" {
  bucket = aws_s3_bucket.bucket.id  # Your bucket name
  key    = "timeswitch/timesswitch.dat"  # Desired S3 destination path with the object name
  source = "./modules/storage/timesswitch.dat"  # Path to the source file in local
}

resource "aws_s3_bucket" "glue_bucket" {
  bucket = "glue-script-dev-akshit"
  force_destroy = true
}

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.glue_bucket.id
  key    = "script/main.py"
  source = "../src/glue/main.py" 
}

output "bucket_id" {
  value = aws_s3_bucket.bucket.id
}

output "glue_bucket" {
  description = "The name of glue bucket"
  value       = aws_s3_bucket.glue_bucket.id
}
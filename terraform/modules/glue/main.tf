terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.47.0"
    }
  }
}

variable "s3_bucket_name" {
  description = "The name of the bucket"
  type        = string
} 

resource "aws_iam_role" "glue" {
  name = "role_glue"
  path = "/service-role/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "glue.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_glue_job" "glueJob" {
  name     = "read_merge_s3_files"
  role_arn = aws_iam_role.glue.arn # replace with your IAM Role that has necessary permissions
  max_capacity = 0.0625

  command {
    name            = "pythonshell"
    python_version  = "3.9"
    script_location = "s3://${var.s3_bucket_name}/script/main.py"
  }

  default_arguments = {
    "--TempDir" = "s3://${var.s3_bucket_name}"
    "--enable-job-insights" = true
    "--enable-continuous-cloudwatch-log" = true
  }
}

output "glue_id" {
  value = aws_iam_role.glue.id
}

output "glue_name" {
  value = aws_glue_job.glueJob.name
}
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.47.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "storage_module" {
    source = "./modules/storage"
}

module "glue_module" {
  source = "./modules/glue"
  s3_bucket_name  = module.storage_module.glue_bucket
}

module "lambda_module" {
  source = "./modules/lambda"
  s3_bucket_name  = module.storage_module.bucket_id
  lambdasVersion = var.lambdasVersion
}

module "policies_module" {
  source = "./modules/policy"
}

resource "aws_iam_role" "sfn_role" {
  name = "sfn_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "states.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach-s3-glue" {
  role       = module.glue_module.glue_id
  policy_arn = module.policies_module.s3_access_policy
}

resource "aws_iam_role_policy_attachment" "attach-s3-lambda" {
  role       = module.lambda_module.lambda_id
  policy_arn = module.policies_module.s3_access_policy
}

resource "aws_iam_role_policy_attachment" "attach-ses-lambda" {
  role       = module.lambda_module.lambda_id
  policy_arn = module.policies_module.send_mail_policy
}

resource "aws_iam_role_policy_attachment" "attach-ssm-lambda" {
  role       = module.lambda_module.lambda_id
  policy_arn = module.policies_module.read_parameters_policy
}

resource "aws_iam_role_policy_attachment" "attach-ssm-glue" {
  role       = module.glue_module.glue_id
  policy_arn = module.policies_module.read_parameters_policy
}

resource "aws_iam_role_policy_attachment" "attach-glueRun-sfn" {
  role       = aws_iam_role.sfn_role.id
  policy_arn = module.policies_module.glue_run_policy
}

resource "aws_iam_role_policy_attachment" "attach-invokeLambda-sfn" {
  role       = aws_iam_role.sfn_role.id
  policy_arn = module.policies_module.invoke_lambda_policy
}

resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "sfn_read_s3_files"
  role_arn = aws_iam_role.sfn_role.arn

  definition = jsonencode({
    Comment = "Read and merge s3 files using AWS Glue",
    StartAt = "File Zipper",
    States = {
      "File Zipper" = {
        Type       = "Task",
        Resource   = "arn:aws:states:::glue:startJobRun.sync",
        Parameters = {
          JobName = "${module.glue_module.glue_name}",
           "Arguments": {
                  "--bucketName" : "${module.storage_module.bucket_id}",
                  "--zipLocation": "zipped/tarrif.zip"
                }
        },
        "OutputPath": "$.Arguments" ,
        Next = "Email Sender"
      },
      "Email Sender" = {
        Type       = "Task",
        Resource   = "${module.lambda_module.lambda_arn}",
        End = true 
      }
    }
  })
}
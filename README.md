# Terraform Glue Task

## Description

- Create a step function using GLUE/PY to read two files from directory tariff/tariff.dat and timeswitch/timesswitch.dat (already provided)
- Write Glue job to read the above two files and create a single zipped file named tariff.zip (same prefix as tariff file with zip extension) and write it back to S3 lolcation zipped/tariff.zip
- Write lambda in TS using SES (simple email service) to pick up the file from the above location and send it as an attachment. 

## Resources
Resources created using tf:

- S3 bucket to store data file
- Upload data files to created bucket
- S3 bucket to store Glue script
- Upload glue script to created bucket
- Step Function role
- Attach glue job run policy and invoke lambda policy to sfn role
- Glue role
- Lambda role
- S3 access policy and attach to Glue role and lambda role
- Attach send mail policy to lambda role
- Define Glue job
- Define Lambda function and corresponding log groups with policy
- Define Step function with 2 states (Glue job for zipping and TS lambda to send mail using SES)

Glue Script:

- Location: src/glue/main.py
- Python shell script which reads data file from s3 bucket, and create a single zipped file and write it back to S3
- ### Testing glue code:
    - Install dependencies `pip install -r src/glue/requirement.txt`
    - Run `python src/glue/test.py`

Typescript lambda:

- Location: src/lambda/index.ts

## Usage

- Pre-requisite: setup `aws configure`
- First initialize the terraform using `terraform init` in 'terraform' folder
- After that, rest of the commands (plan and apply) are stored in shell script 'deploy.sh'
- Add sender and receiver's mail in 'deploy.sh'
- 'deploy.sh': this shell script abstracts the zipping of typescript package along with its dependencies
- Secondly, execute shell script `sh deploy.sh`
